require_relative './spec_helper.rb'
require_relative '../pattern.rb'

big_diamond = "    *    
   ***   
  *****  
 ******* 
*********
 ******* 
  *****  
   ***   
    *    "

size_5_diamond = "  *  
 *** 
*****
 *** 
  *  "

count_2_size_7_vert_chain = "   *   
  ***  
 ***** 
*******
 ***** 
  ***  
   *   
   *   
  ***  
 ***** 
*******
 ***** 
  ***  
   *   "

count_4_size_3_horiz_chain = " *   *   *   * 
*** *** *** ***
 *   *   *   * "

describe "get_diamond" do

  diamond = get_diamond

  it 'returns a single string' do
    expect(diamond.class).to eq(String), "make sure `get_diamond` returns a single string!"
  end

  it '`puts` out a correctly formatted 9 row diamond' do
    expect(diamond).to eq(big_diamond), "expected a diamond with 9 rows, tapering in each direction. Received:\n#{diamond}"
  end

end

describe "get_custom_height_diamond" do

  diamond = get_custom_height_diamond(5)

  it 'returns a single string' do
    expect(diamond.class).to eq(String), "make sure `get_custom_height_diamond` returns a single string!"
  end

  it '`puts` out a correctly formatted diamond with custom height' do
    expect(diamond).to eq(size_5_diamond), "expected a diamond with 5 rows, tapering in each direction. Received:\n#{diamond}"
  end

end

describe "get_chained_diamonds" do

  diamonds = get_chained_diamonds(2, 7)

  it 'returns a single string' do
    expect(diamonds.class).to eq(String), "make sure `get_chained_diamonds` returns a single string!"
  end

  it '`puts` out a correctly formatted vertical diamond chain with custom diamond count and height' do
    expect(diamonds).to eq(count_2_size_7_vert_chain), "expected two vertically chained diamonds with 7 rows each. Received:\n#{diamonds}"
  end

end

describe "get_side_by_side_diamonds" do

  diamonds = get_side_by_side_diamonds(4, 3)

  it 'returns a single string' do
    expect(diamonds.class).to eq(String), "make sure `get_side_by_side_diamonds` returns a single string!"
  end

  it '`puts` out a correctly formatted vertical diamond chain with custom diamond count and height' do
    expect(diamonds).to eq(count_4_size_3_horiz_chain), "expected four horizontally chained diamonds with 3 rows each. Received:\n#{diamonds}"
  end

end
