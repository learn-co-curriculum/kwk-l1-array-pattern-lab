def get_diamond
  # your code here!
  # see part I of the readme!
end

def get_custom_height_diamond
  # your code here!
  # see part II of the readme!
  # don't forget the height parameter!
end

def get_chained_diamonds
  # your code here!
  # see part III of the readme!
  # don't forget the count and height parameters!
end

def get_side_by_side_diamonds(count, height)
  # your code here!
  # see part IV of the readme!
  # don't forget the count and height parameters!
end
