## Pattern Building

***This is a group lab, so make sure you are working in a group of 2-4 students***

Patterns are all around us. From architecture to fashion, patterns can be seen everywhere and are pleasing to the eye. How can we use computers to generate patterns?

![patterns](https://s3.amazonaws.com/upperline/curriculum-assets/patterns-everywhere.png)

Your mission is to create a series of patterns using Ruby.  

### Part I: The Diamond
Implement the `get_diamond` method. When we `puts` the return value of `get_diamond`, we should see this printed out:

```
    *    
   ***   
  *****  
 ******* 
*********
 ******* 
  *****  
   ***   
    *    
```

The middle row in the diamond above contains 9 \*'s. Each subsequent row has two less stars (one from each side). If your diamond is a single string, don't forget the "\n" escape character, which designates a newline in a string, i.e.:

```ruby
puts "foo\nbar"
foo
bar
```
### IMPORTANT:

**Whitespaces on each side:** Each row should be the same character width as the middle row. This means that there are trailing whitespaces on every row except for the widest (middle) row. As an example, with our first row in the above 9 height diamond:

```ruby
# correct - with trailing whitespace:
"    *    "

# incorrect - no trailing whitespace:
"    *"
```

**Use Arrays:** While the methods final return value should be a string, your methods should make use of Ruby `Array`s. For example, with a single diamond, each array element should be a single line of the diamond, i.e.:

```ruby
diamond = [" * ", "***", " * "]
```

Understanding that the method should return a single string, how could we use the [`array.join` method][array-join] to turn our array into a string?

### Part II: User Specified Height

Implement the `get_custom_height_diamond` method that takes in a single argument: an integer that determines how tall (how many rows) the diamond should be. The argument should be a non-negative integer less than 24 (we don't want to make the diamond too big). The method should return a diamond as a single string accurate to the specification.

If `get_custom_height_diamond` was called with an argument of  7, it should return the following `puts`-able diamond as a string:

```
   *
  ***
 *****
*******
 *****
  ***
   *   
```

Notice: we are sticking with the same pattern. The middle row of the diamond is the largest, and each subsequent row is two less than before. All diamonds should have **odd** height values! That way, we can ensure their top and bottom tips are always single stars.

### Part III: Chained Diamonds

Implement the `get_chained_diamonds` method, which takes two arguments: the number of diamonds and the desired height of the diamonds (0 < int < 24). After creating all of the diamonds, the `get_chained_diamonds` should return a single string of all the diamonds. This would be an example of the result after calling `get_chained_diamonds` with 3 as the first argument and 7 as the second argument:

```
   *
  ***
 *****
*******
 *****
  ***   
   *
   *
  ***
 *****
*******
 *****
  ***   
   *
   *
  ***
 *****
*******
 *****
  ***   
   *
```

Consider re-using a method you have already built out within the `get_chained_diamonds` method!

### Part IV: Side-by-Side

Implement the `get_side_by_side_diamonds` method, which should take two arguments: the number of diamonds and the height of the diamonds (0 < int < 24). The method should return a single string. Note the single space between the middle rows of each diamond!:


<img src="https://s3.amazonaws.com/upperline/curriculum-assets/patterns-model.jpg" width="150" align="left" style="margin-right:15px">



```
   *       *       *
  ***     ***     ***
 *****   *****   *****
******* ******* *******
 *****   *****   *****
  ***     ***     ***
   *       *       *
```
<br>


### Part V Bonus: Rainbow Magic

<img src="https://s3.amazonaws.com/upperline/curriculum-assets/patterns-colored-skirt.jpg" width="200" align="right" style="margin-right:15px">

We have included a Ruby gem that allows you to add colors to your strings. Once you have passed all the tests, see how you can use the `colorize` gem to format your diamonds' colors! Make sure to add `require 'colorize'` to the top of your `pattern.rb` file. To get started, trying `puts`ing out a string like this:

```ruby
puts "I am a green string!".colorize(:green)
```

Take a look at the [documentation][colorize] for further instructions. See if you can't print out a 7 row diamond with all of the colors of the rainbow.

<sup>Inspiration for this lab comes from one of the labs in Harvard's CS50 class (Intro to Computer Science)</sup>

[colorize]: https://github.com/fazibear/colorize 
[array-join]: https://ruby-doc.org/core-2.2.0/Array.html#method-i-join
